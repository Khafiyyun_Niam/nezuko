<?php
/**
 * Filename: Kernel.php
 * Last modified: 2/2/20, 6:49 AM
 *
 * @author Panji Setya Nur Prawira
 * @facebook https://www.facebook.com/PanjiNamjaElf
 * @twitter https://twitter.com/PanjiNamjaElf18
 * @github https://github.com/PanjiNamjaElf/
 * @gitlab https://gitlab.com/PanjiNamjaElf
 * @copyright (c) 2020
 */

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        // $schedule->command('inspire')
        //          ->hourly();
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
