<?php
 /**
 * Filename: 2014_10_12_000000_create_users_table.php
 * Last modified: 2/2/20, 4:48 PM
 *
 * @author Panji Setya Nur Prawira
 * @facebook https://www.facebook.com/PanjiNamjaElf
 * @twitter https://twitter.com/PanjiNamjaElf18
 * @github https://github.com/PanjiNamjaElf/
 * @gitlab https://gitlab.com/PanjiNamjaElf
 * @copyright (c) 2020
 */

 use Illuminate\Database\Migrations\Migration;
 use Illuminate\Database\Schema\Blueprint;
 use Illuminate\Support\Facades\Schema;

 class CreateUsersTable extends Migration {
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up() {
   Schema::create('users', function(Blueprint $table) {
    $table->bigIncrements('id');
    $table->string('name');
    $table->string('username');
    $table->string('password');
    $table->rememberToken();
    $table->timestamps();
   });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down() {
   Schema::dropIfExists('users');
  }
 }
