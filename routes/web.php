<?php
 /**
 * Filename: web.php
 * Last modified: 2/2/20, 5:19 PM
 *
 * @author Panji Setya Nur Prawira
 * @facebook https://www.facebook.com/PanjiNamjaElf
 * @twitter https://twitter.com/PanjiNamjaElf18
 * @github https://github.com/PanjiNamjaElf/
 * @gitlab https://gitlab.com/PanjiNamjaElf
 * @copyright (c) 2020
 */
 /*
 |--------------------------------------------------------------------------
 | Web Routes
 |--------------------------------------------------------------------------
 |
 | Here is where you can register web routes for your application. These
 | routes are loaded by the RouteServiceProvider within a group which
 | contains the "web" middleware group. Now create something great!
 |
 */

 /*
 |------------------------------------------
 |  Website (Not Authorized)
 |------------------------------------------
 */
 Route::group(['before'     => 'auth',
               'middleware' => 'guest'
              ], function() {

  # Authentication
  Route::group(['prefix' => 'auth'], function() {
   # Login
   Route::get('login', 'Auth\LoginController@showLoginForm')->name('login');
   Route::post('login', 'Auth\LoginController@login');
  });
 });

 /*
 |------------------------------------------
 | Website (Authorized)
 |------------------------------------------
 */
 Route::group(['middleware' => 'auth'], function() {
  # General
  Route::get('/', function() {
   return View('dashboard');
  })->name('dashboard');
  Route::post('auth/logout', 'Auth\LoginController@logout')->name('logout');
 });
